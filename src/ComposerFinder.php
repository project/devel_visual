<?php

declare(strict_types = 1);

namespace Drupal\devel_visual;

class ComposerFinder {

  public function getComposerJson(string $absoluteDirectory): string {
    $fileName = $this->getFileName();
    $dir = Utils::findFileUpward($fileName, $absoluteDirectory);

    return $dir ? "$dir/$fileName" : '';
  }

  public function getComposerLock(string $absoluteDirectory): string {
    $fileName = preg_replace('/\.json$/', '.lock', $this->getFileName());
    $dir = Utils::findFileUpward($fileName, $absoluteDirectory);

    return $dir ? "$dir/$fileName" : '';
  }

  protected function getFileName(): string {
    return getenv('COMPOSER') ?: 'composer.json';
  }

}
