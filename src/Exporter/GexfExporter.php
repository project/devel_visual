<?php

declare(strict_types = 1);

namespace Drupal\devel_visual\Exporter;

use DateTimeImmutable;
use DateTimeInterface;
use DOMDocument;
use Fhaculty\Graph\Exporter\ExporterInterface;
use Fhaculty\Graph\Edge\Base as Edge;
use Fhaculty\Graph\Graph;
use Fhaculty\Graph\Vertex;

/**
 * @todo Move this out into an external library.
 */
class GexfExporter implements ExporterInterface {

  /**
   * @var \DateTimeInterface
   */
  protected $now;

  /**
   * @var \Fhaculty\Graph\Graph
   */
  protected $graph;

  /**
   * @var \DOMDocument
   */
  protected $doc;

  /**
   * @var \DOMElement
   */
  protected $rootElement;

  /**
   * @var \DOMElement
   */
  protected $graphElement;

  /**
   * @var \DOMElement
   */
  protected $nodesElement;

  /**
   * @var \DOMElement
   */
  protected $edgesElement;

  /**
   * @var string
   */
  protected $gexfVersion = '1.2draft';

  public function __construct(?DateTimeInterface $now = NULL) {
    $this->now = $now ?: new DateTimeImmutable();
  }

  /**
   * {@inheritdoc}
   *
   * @return string
   */
  public function getOutput(Graph $graph) {
    $this->graph = $graph;

    return $this
      ->initDoc()
      ->initMeta()
      ->initGraph()
      ->addNodes()
      ->addEdges()
      ->doc
      ->saveXML();
  }

  /**
   * @return $this
   */
  protected function initDoc() {
    $this->doc = new DOMDocument('1.0', 'UTF-8');
    $this->doc->formatOutput = TRUE;
    $this->doc->preserveWhiteSpace = TRUE;

    $this->rootElement = $this->doc->createElement('gexf');
    $this->doc->appendChild($this->rootElement);

    $this->rootElement->setAttribute('version', $this->gexfVersion);
    $this->rootElement->setAttribute('xmlns', "http://www.gexf.net/{$this->gexfVersion}");

    $this->rootElement->setAttributeNS(
      'http://www.w3.org/2000/xmlns/',
      'xmlns:viz',
      'http://www.gexf.net/1.2draft/viz'
    );

    $this->rootElement->setAttributeNS(
      'http://www.w3.org/2000/xmlns/',
      'xmlns:xsi',
      'http://www.w3.org/2001/XMLSchema-instance'
    );

    $this->rootElement->setAttributeNS(
      'http://www.w3.org/2001/XMLSchema-instance',
      'schemaLocation',
      "http://www.gexf.net/1.2draft https://gephi.org/gexf/{$this->gexfVersion}/gexf.xsd"
    );

    return $this;
  }

  /**
   * @return $this
   */
  protected function initMeta() {
    $meta = $this->doc->createElement('meta');
    $this->rootElement->appendChild($meta);

    $meta->setAttribute('lastmodifieddate', $this->now->format('Y-m-d'));
    $meta->appendChild($this->doc->createElement('creator', 'drupal/devel_visual'));
    $meta->appendChild($this->doc->createElement('description', ''));

    return $this;
  }

  /**
   * @return $this
   */
  protected function initGraph() {
    $this->graphElement = $this->doc->createElement('graph');
    $this->graphElement->setAttribute('mode', 'static');
    $this->graphElement->setAttribute('defaultedgetype', 'directed');
    $this->rootElement->appendChild($this->graphElement);

    return $this;
  }

  /**
   * @return $this
   */
  protected function addNodes() {
    $this->nodesElement = $this->doc->createElement('nodes');
    $this->graphElement->appendChild($this->nodesElement);

    foreach ($this->graph->getVertices() as $vertex) {
      $this->addNode($vertex);
    }

    return $this;
  }

  /**
   * @return $this
   */
  protected function addNode(Vertex $vertex) {
    $element = $this->doc->createElement('node');
    $this->nodesElement->appendChild($element);

    $element->setAttribute('id', $vertex->getId());
    $element->setAttribute('label', $vertex->getId());

    return $this;
  }

  /**
   * @return $this
   */
  protected function addEdges() {
    $this->edgesElement = $this->doc->createElement('edges');
    $this->graphElement->appendChild($this->edgesElement);

    foreach ($this->graph->getEdges() as $edge) {
      $this->addEdge($edge);
    }

    return $this;
  }

  /**
   * @return $this
   */
  protected function addEdge(Edge $edge) {
    $sourceVertices = $edge->getVerticesStart()->getIds();
    $targetVertices = $edge->getVerticesTarget()->getIds();
    $count = min(count($sourceVertices), count($targetVertices));

    for ($i = 0; $i < $count; $i++) {
      $source = $this->graph->getVertex($sourceVertices[$i]);
      $target = $this->graph->getVertex($targetVertices[$i]);

      $element = $this->doc->createElement('edge');
      $this->edgesElement->appendChild($element);

      $element->setAttribute('id', $edge->getAttribute('id'));
      $element->setAttribute('label', $edge->getAttribute('id'));
      $element->setAttribute('source', $source->getId());
      $element->setAttribute('target', $target->getId());
    }

    return $this;
  }

}
