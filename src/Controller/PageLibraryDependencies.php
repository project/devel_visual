<?php

declare(strict_types = 1);

namespace Drupal\devel_visual\Controller;

use Symfony\Component\DependencyInjection\ContainerInterface;

class PageLibraryDependencies extends PageDependenciesBase {

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('devel_visual.graph_exporter.graphviz_dot'),
      $container->get('devel_visual.graph_provider.library_dependencies')
    );
  }

}
