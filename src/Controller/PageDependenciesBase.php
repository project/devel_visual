<?php

declare(strict_types = 1);

namespace Drupal\devel_visual\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\devel_visual\GraphProviderInterface;
use Fhaculty\Graph\Exporter\ExporterInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class PageDependenciesBase extends ControllerBase {

  /**
   * @var \Drupal\devel_visual\GraphProviderInterface
   */
  protected $graphProvider;

  /**
   * @var \Fhaculty\Graph\Exporter\ExporterInterface
   */
  protected $dotExporter;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('devel_visual.graph_exporter.graphviz_dot'),
      $container->get('devel_visual.graph_provider.library_dependencies')
    );
  }

  public function __construct(
    ExporterInterface $dotExporter,
    GraphProviderInterface $graphProvider
  ) {
    $this->dotExporter = $dotExporter;
    $this->graphProvider = $graphProvider;
  }

  public function content(): array {
    return [
      'graph' => [
        '#markup' => '<div id="my-graph-01"/>',
        '#attached' => [
          'library' => [
            'devel_visual/d3-graphviz.integration',
          ],
          'drupalSettings' => [
            'develVisual' => [
              'd3GraphViz' => [
                'myGraph01' => [
                  'selector' => '#my-graph-01',
                  'source' => [
                    'type' => 'dot',
                    'value' => $this->dotExporter->getOutput($this->graphProvider->getGraph()),
                  ],
                ],
              ],
            ],
          ],
        ],
      ],
    ];
  }

}
