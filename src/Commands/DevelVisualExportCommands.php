<?php

declare(strict_types = 1);

namespace Drupal\devel_visual\Commands;

use Consolidation\AnnotatedCommand\CommandData;
use Consolidation\AnnotatedCommand\CommandResult;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\devel_visual\Formatter\GexfFormatter;
use Drupal\devel_visual\Formatter\GraphVizDotFormatter;
use Drupal\devel_visual\Formatter\GraphVizSvgFormatter;
use Drupal\devel_visual\GraphProviderInterface;
use Drush\Commands\DrushCommands;
use League\Container\ContainerAwareInterface;
use League\Container\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

class DevelVisualExportCommands extends DrushCommands implements
    ContainerInjectionInterface,
    ContainerAwareInterface {

  use ContainerAwareTrait;

  /**
   * @var \Drupal\devel_visual\GraphProviderInterface
   */
  protected $moduleDependencies;

  /**
   * @var \Drupal\devel_visual\GraphProvider\LibraryDependencies
   */
  protected $libraryDependencies;

  /**
   * @var \Drupal\devel_visual\GraphProviderInterface
   */
  protected $composerPackageDependencies;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('@devel_visual.graph_provider.module_dependencies'),
      $container->get('@devel_visual.graph_provider.library_dependencies'),
      $container->get('@devel_visual.graph_provider.composer_package_dependencies')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(
    GraphProviderInterface $moduleDependencies,
    GraphProviderInterface $libraryDependencies,
    GraphProviderInterface $composerPackageDependencies
  ) {
    parent::__construct();
    $this->moduleDependencies = $moduleDependencies;
    $this->libraryDependencies = $libraryDependencies;
    $this->composerPackageDependencies = $composerPackageDependencies;
  }

  /**
   * @hook pre-command @devel_visual-init-graph_formatters
   */
  public function initGraphFormatters(CommandData $commandData) {
    /** @var \Drush\Formatters\DrushFormatterManager $fm */
    $fm = $this
      ->container
      ->get('formatterManager');

    $pairs = [
      'graphviz-dot' => GraphVizDotFormatter::class,
      'graphviz-svg' => GraphVizSvgFormatter::class,
      'gexf' => GexfFormatter::class,
    ];

    foreach ($pairs as $name => $class) {
      if (!$fm->hasFormatter($name)) {
        $fm->addFormatter($name, new $class());
      }
    }
  }

  /**
   * @command devel_visual:export:module-graph
   * @bootstrap full
   *
   * @devel_visual-init-graph_formatters
   */
  public function exportModuleGraph(
    array $options = [
      'format' => 'graphviz-dot',
    ]
  ): CommandResult {
    return CommandResult::dataWithExitCode(
      $this->moduleDependencies->getGraph(),
      0
    );
  }

  /**
   * @command devel_visual:export:library-graph
   * @bootstrap full
   *
   * @devel_visual-init-graph_formatters
   */
  public function exportLibraryGraph(
    array $options = [
      'format' => 'graphviz-dot',
    ]
  ): CommandResult {
    return CommandResult::dataWithExitCode(
      $this->libraryDependencies->getGraph(),
      0
    );
  }

  /**
   * @command devel_visual:export:composer-graph
   * @bootstrap full
   *
   * @devel_visual-init-graph_formatters
   */
  public function exportComposerGraph(
    array $options = [
      'format' => 'graphviz-dot',
    ]
  ): CommandResult {
    return CommandResult::dataWithExitCode(
      $this->composerPackageDependencies->getGraph(),
      0
    );
  }

}
