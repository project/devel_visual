<?php

declare(strict_types = 1);

namespace Drupal\devel_visual\Formatter;

use Consolidation\OutputFormatters\Formatters\FormatterInterface;
use Consolidation\OutputFormatters\Options\FormatterOptions;
use Consolidation\OutputFormatters\Validate\ValidationInterface;
use Drupal\devel_visual\Exporter\GexfExporter;
use Fhaculty\Graph\Graph;
use ReflectionClass;
use Symfony\Component\Console\Output\OutputInterface;

class GexfFormatter implements FormatterInterface, ValidationInterface {

  /**
   * {@inheritdoc}
   */
  public function write(OutputInterface $output, $data, FormatterOptions $options) {
    $exporter = new GexfExporter();
    $output->write($exporter->getOutput($data));
  }

  /**
   * {@inheritdoc}
   */
  public function isValidDataType(ReflectionClass $dataType) {
    return $dataType->name === Graph::class;
  }

  /**
   * {@inheritdoc}
   */
  public function validate($structuredData) {
    return $structuredData;
  }

}
