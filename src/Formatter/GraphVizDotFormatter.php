<?php

declare(strict_types = 1);

namespace Drupal\devel_visual\Formatter;

use Consolidation\OutputFormatters\Formatters\FormatterInterface;
use Consolidation\OutputFormatters\Options\FormatterOptions;
use Consolidation\OutputFormatters\Validate\ValidationInterface;
use Fhaculty\Graph\Graph;
use Graphp\GraphViz\Dot;
use ReflectionClass;
use Symfony\Component\Console\Output\OutputInterface;

class GraphVizDotFormatter implements FormatterInterface, ValidationInterface {

  /**
   * {@inheritdoc}
   */
  public function write(OutputInterface $output, $data, FormatterOptions $options) {
    $exporter = new Dot();
    $output->write($exporter->getOutput($data));
  }

  /**
   * {@inheritdoc}
   */
  public function isValidDataType(ReflectionClass $dataType) {
    return $dataType->name === Graph::class;
  }

  /**
   * {@inheritdoc}
   */
  public function validate($structuredData) {
    return $structuredData;
  }

}
