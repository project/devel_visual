<?php

declare(strict_types = 1);

namespace Drupal\devel_visual\GraphProvider;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\devel_visual\GraphProviderInterface;
use Fhaculty\Graph\Edge\Directed;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class ModuleDependencies extends GraphProviderBase implements GraphProviderInterface, ContainerInjectionInterface {

  /**
   * {@inheritdoc}
   */
  const EVENT_ALTER = 'devel_visual.graph_provider.module_dependencies.alter';

  /**
   * {@inheritdoc}
   */
  protected $graphName = 'module_dependencies';

  /**
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  protected $moduleExtensionList;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('event_dispatcher'),
      $container->get('extension.list.module')
    );
  }

  public function __construct(
    EventDispatcherInterface $eventDispatcher,
    ModuleExtensionList $moduleExtensionList
  ) {
    parent::__construct($eventDispatcher);
    $this->moduleExtensionList = $moduleExtensionList;
  }

  /**
   * {@inheritdoc}
   */
  public function buildGraph() {
    $modules = $this->moduleExtensionList->reset()->getList();
    foreach ($modules as $fromName => $module) {
      // @todo Add vertex attributes:
      // - *.info.yml/package
      // - *.info.yml/project
      // - version
      // - composer.json/name
      // - directory: core, contrib, custom
      // - status: enabled|disabled
      // - experimental: true|false
      // - test: true|false
      $fromVertex = $this->graph->createVertex($fromName, TRUE);

      if (empty($module->info['dependencies'])) {
        continue;
      }

      foreach ($this->parseModuleNamesFromDependencies($module->info['dependencies']) as $toName) {
        $edge = new Directed(
          $fromVertex,
          $this->graph->createVertex($toName, TRUE)
        );
        $edge->setAttribute('id', "{$fromName}-{$toName}");
      }
    }

    return $this;
  }

  protected function parseModuleNamesFromDependencies(array $dependencies): array {
    $moduleNames = [];

    foreach ($dependencies as $dependency) {
      $moduleNames[] = $this->parseModuleNameFromDependency($dependency);
    }

    return $moduleNames;
  }

  protected function parseModuleNameFromDependency(string $dependency): string {
    $pattern = '/^((?P<vendor>[^:]+):){0,1}(?P<name>[^\s]+)(\s+(?P<version>.+)){0,1}$/';

    $matches = ['name' => ''];
    preg_match($pattern, $dependency, $matches);

    return $matches['name'];
  }

}
