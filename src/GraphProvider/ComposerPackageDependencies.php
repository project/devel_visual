<?php

declare(strict_types = 1);

namespace Drupal\devel_visual\GraphProvider;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\devel_visual\GraphProviderInterface;
use Drupal\devel_visual\ComposerFinder;
use Fhaculty\Graph\Edge\Directed;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class ComposerPackageDependencies extends GraphProviderBase implements GraphProviderInterface, ContainerInjectionInterface {

  const EVENT_ALTER = 'devel_visual.graph_provider.composer_package_dependencies.alter';

  /**
   * @var string
   */
  protected $graphName = 'composer_package_dependencies';

  /**
   * @var \Drupal\devel_visual\ComposerFinder
   */
  protected $composerFinder;

  /**
   * @var array
   */
  protected $json = [];

  /**
   * @var array
   */
  protected $lock = [];

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('event_dispatcher'),
      $container->get('devel_visual.composer_finder')
    );
  }

  public function __construct(
    EventDispatcherInterface $eventDispatcher,
    ComposerFinder $composerFinder
  ) {
    parent::__construct($eventDispatcher);
    $this->composerFinder = $composerFinder;
  }

  /**
   * {@inheritdoc}
   */
  public function buildGraph() {
    $this
      ->initSource()
      ->addPackages($this->lock['packages'] ?? [], 'prod')
      ->addPackages($this->lock['packages-dev'] ?? [], 'prod')
      ->addJson($this->json['require'], 'prod')
      ->addJson($this->json['require-dev'], 'dev');

    return $this;
  }

  /**
   * @return $this
   */
  protected function initSource() {
    $this->json = [];
    $this->lock = [];

    $jsonFileName = $this->composerFinder->getComposerJson(DRUPAL_ROOT);
    $lockFileName = $this->composerFinder->getComposerLock(DRUPAL_ROOT);

    if ($jsonFileName) {
      $this->json = (array) json_decode(file_get_contents($jsonFileName), TRUE);
    }

    $this->json += [
      'name' => $this->getPackageNameFromDirectory(DRUPAL_ROOT),
      'require' => [],
      'require-dev' => [],
    ];

    if ($lockFileName) {
      $this->lock = (array) json_decode(file_get_contents($lockFileName), TRUE);
    }

    $this->lock += [
      'packages' => [],
      'packages-dev' => [],
    ];

    return $this;
  }

  /**
   * @return $this
   */
  protected function addPackages(array $packages, string $relationship) {
    foreach ($packages as $from) {
      $fromVertex = $this->graph->createVertex($from['name'], TRUE);
      $fromVertex->setAttribute('label', $from['name']);
      $fromVertex->setAttribute('type', $from['type'] ?? 'library');
      $fromVertex->setAttribute('version', ltrim($from['version'], 'v'));

      if (empty($from['require'])) {
        continue;
      }

      foreach ($from['require'] as $toName => $toVersion) {
        $toVertex = $this->graph->createVertex($toName, TRUE);

        $edge = new Directed($fromVertex, $toVertex);
        $edge->setAttribute('id', "{$from['name']}-{$toName}");
        $edge->setAttribute('version', $toVersion);
        $edge->setAttribute('relationship', $relationship);
      }
    }

    return $this;
  }

  /**
   * @return $this
   */
  protected function addJson(array $packages, string $relationship) {
    $fromVertex = $this->graph->createVertex($this->json['name'], TRUE);
    foreach ($packages as $toName => $edgeVersion) {
      $toVertex = $this->graph->createVertex($toName, TRUE);

      $edge = new Directed($fromVertex, $toVertex);
      $edge->setAttribute('id', "{$this->json['name']}-{$toName}-$relationship");
      $edge->setAttribute('version', $edgeVersion);
      $edge->setAttribute('relationship', $relationship);
    }

    return $this;
  }

  protected function getPackageNameFromDirectory(string $absoluteDir): string {
    $parts = explode(DIRECTORY_SEPARATOR, $absoluteDir);

    return implode('/', array_slice($parts, -2));
  }

}
