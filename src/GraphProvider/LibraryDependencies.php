<?php

declare(strict_types = 1);

namespace Drupal\devel_visual\GraphProvider;

use Drupal\Core\Asset\LibraryDiscoveryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Extension\ThemeHandlerInterface;
use Drupal\devel_visual\GraphProviderInterface;
use Fhaculty\Graph\Edge\Directed;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class LibraryDependencies extends GraphProviderBase implements GraphProviderInterface, ContainerInjectionInterface {

  /**
   * {@inheritdoc}
   */
  const EVENT_ALTER = 'devel_visual.graph_provider.library_dependencies.alter';

  /**
   * {@inheritdoc}
   */
  protected $graphName = 'library_dependencies';

  /**
   * @var \Drupal\Core\Asset\LibraryDiscoveryInterface
   */
  protected $libraryDiscovery;

  /**
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * @var \Drupal\Core\Extension\ThemeHandlerInterface
   */
  protected $themeHandler;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('event_dispatcher'),
      $container->get('library.discovery'),
      $container->get('module_handler'),
      $container->get('theme_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(
    EventDispatcherInterface $eventDispatcher,
    LibraryDiscoveryInterface $libraryDiscovery,
    ModuleHandlerInterface $moduleHandler,
    ThemeHandlerInterface $themeHandler
  ) {
    parent::__construct($eventDispatcher);
    $this->libraryDiscovery = $libraryDiscovery;
    $this->moduleHandler = $moduleHandler;
    $this->themeHandler = $themeHandler;
  }

  /**
   * {@inheritdoc}
   */
  public function buildGraph() {
    $libraries = $this->getLibraries();
    foreach ($libraries as $fromId => $library) {
      $fromVertex = $this->graph->createVertex($fromId, TRUE);
      $fromVertex->setAttribute('label', $fromId);
      $fromVertex->setAttribute('missing', FALSE);
      $fromVertex->setAttribute('sourceType', $library['sourceType']);

      if (empty($library['dependencies'])) {
        continue;
      }

      foreach ($library['dependencies'] as $toId) {
        $toVertex = $this->graph->createVertex($toId, TRUE);
        $toVertex->setAttribute('label', $toId);
        $toVertex->setAttribute('missing', !isset($libraries[$toId]));
        $toVertex->setAttribute('sourceType', 'unknown');

        $edge = new Directed($fromVertex, $toVertex);
        $edge->setAttribute('id', "{$fromId}-{$toId}");
      }
    }

    return $this;
  }

  protected function getLibraries() {
    $libraries = [];
    $sources = [
      'core' => ['core'],
      'module' => array_keys($this->moduleHandler->getModuleList()),
      'theme' => array_keys($this->themeHandler->listInfo()),
    ];
    foreach ($sources as $sourceType => $extensionNames) {
      foreach ($extensionNames as $extensionName) {
        $libs = $this->libraryDiscovery->getLibrariesByExtension($extensionName);
        foreach ($libs as $libName => $lib) {
          $libId = "$extensionName/$libName";
          $libraries[$libId] = $lib + [
            'id' => $libId,
            'sourceType' => $sourceType,
          ];
        }
      }
    }

    return $libraries;
  }

}
