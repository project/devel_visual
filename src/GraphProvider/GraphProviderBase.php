<?php

declare(strict_types = 1);

namespace Drupal\devel_visual\GraphProvider;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\devel_visual\GraphProviderEvent;
use Drupal\devel_visual\GraphProviderInterface;
use Fhaculty\Graph\Graph;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

abstract class GraphProviderBase implements GraphProviderInterface, ContainerInjectionInterface {

  const EVENT_ALTER = '';

  /**
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * @var string
   */
  protected $graphName = '';

  /**
   * @var string
   */
  protected $graphRankDir = 'LR';

  /**
   * @var \Fhaculty\Graph\Graph
   */
  protected $graph;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('event_dispatcher')
    );
  }

  public function __construct(
    EventDispatcherInterface $eventDispatcher
  ) {
    $this->eventDispatcher = $eventDispatcher;
  }

  /**
   * {@inheritdoc}
   */
  public function getGraph(): Graph {
    return $this
      ->initGraph()
      ->buildGraph()
      ->alterGraph()
      ->graph;
  }

  /**
   * @return $this
   */
  protected function initGraph() {
    $this->graph = new Graph();
    $this->graph->setAttribute('graphviz.graph.name', $this->graphName);
    $this->graph->setAttribute('graphviz.graph.rankdir', $this->graphRankDir);

    return $this;
  }

  /**
   * @return $this
   */
  abstract protected function buildGraph();

  protected function alterGraph() {
    $event = new GraphProviderEvent();
    $event->graph = $this->graph;
    $this->eventDispatcher->dispatch(static::EVENT_ALTER, $event);

    return $this;
  }

}
