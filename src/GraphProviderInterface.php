<?php

declare(strict_types = 1);

namespace Drupal\devel_visual;

use Fhaculty\Graph\Graph;

interface GraphProviderInterface {

  public function getGraph(): Graph;

}
