<?php

declare(strict_types = 1);

namespace Drupal\devel_visual;

use Symfony\Component\EventDispatcher\Event;

class GraphProviderEvent extends Event {

  /**
   * @var \Fhaculty\Graph\Graph
   */
  public $graph;

}
