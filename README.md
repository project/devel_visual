# Development - Visualizer

Currently supported graphs:
* Module dependencies
* Library dependencies
* Composer package dependencies

@todo
* Configuration dependencies
* Migration dependencies
* Entity reference fields
* Database schema


## Install

1. Run `cd my/project/root`
1. Run `composer install drupal/devel_visual`
1. Run `vendor/bin/drush pm-enable devel_visual`
1. Go to `http://localhost/admin/reports/devel_visual`
1. Run `vendor/bin/drush list --filter=devel_visual`
1. Have fun :-)
