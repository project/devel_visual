/**
 * @file
 * Foo.
 */

(function (Drupal) {

  Drupal.behaviors.develVisualD3Graphviz = {
    attach: function (context, settings) {
      if (!settings.hasOwnProperty('develVisual')
        || !settings.develVisual.hasOwnProperty('d3GraphViz')
      ) {
        return;
      }

      for (let name in settings.develVisual.d3GraphViz) {
        if (settings.develVisual.d3GraphViz.hasOwnProperty(name)) {
          Drupal.develVisualD3Graphviz.init(name, settings.develVisual.d3GraphViz[name]);
        }
      }
    }
  };

  Drupal.develVisualD3Graphviz = Drupal.develVisualD3Graphviz || {};

  Drupal.develVisualD3Graphviz.init = function (name, config) {
    d3.select(config.selector)
      .graphviz()
      .renderDot(config.source.value);
  };

})(Drupal);
